'''
may 6th 2019 monday
'''
from __future__ import division
import math
import time
import tqdm
import torch
import torch.nn as nn
import torch.nn.functional as F
from torch.autograd import Variable
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.patches as patches


def to_cpu(tensor):
    return tensor.detach().cpu()


def load_classes(path):
    """
    Loads class labels at 'path'

    params:
    - path: (str) path of .names file which contains
            classes
    """
    fp = open(path, "r")
    names = fp.read().split("\n")[:-1]
    return names


def weights_init_normal(m):
    classname = m.__class__.__name__
    if classname.find("Conv") != -1:
        torch.nn.init.normal_(m.weight.data, 0.0, 0.02)
    elif classname.find("BatchNorm2d") != -1:
        torch.nn.init.normal_(m.weight.data, 1.0, 0.02)
        torch.nn.init.constant_(m.bias.data, 0.0)


def rescale_boxes(boxes, current_dim, original_shape):
    """ Rescales bounding boxes to the original shape """
    orig_h, orig_w = original_shape
    # The amount of padding that was added
    pad_x = max(orig_h - orig_w, 0) * (current_dim / max(original_shape))
    pad_y = max(orig_w - orig_h, 0) * (current_dim / max(original_shape))
    # Image height and width after padding is removed
    unpad_h = current_dim - pad_y
    unpad_w = current_dim - pad_x
    # Rescale bounding boxes to dimension of original image
    boxes[:, 0] = ((boxes[:, 0] - pad_x // 2) / unpad_w) * orig_w
    boxes[:, 1] = ((boxes[:, 1] - pad_y // 2) / unpad_h) * orig_h
    boxes[:, 2] = ((boxes[:, 2] - pad_x // 2) / unpad_w) * orig_w
    boxes[:, 3] = ((boxes[:, 3] - pad_y // 2) / unpad_h) * orig_h
    return boxes


def xywh2xyxy(x):
    """
    Transform bbox coordinates
    
      |---------|         *---------*
      |         |         |         |
      |  (x,y)  h         |         |
      |         |         |         |
      |____w____|         *_________*
         center             corners

    params:
    - x: (Tensor) bbox with size [..., 4]

    Returns
    - y: (Tensor) bbox with size [..., 4]
    """
    y = x.new(x.shape)
    y[..., 0] = x[..., 0] - x[..., 2] / 2
    y[..., 1] = x[..., 1] - x[..., 3] / 2
    y[..., 2] = x[..., 0] + x[..., 2] / 2
    y[..., 3] = x[..., 1] + x[..., 3] / 2
    return y


def ap_per_class(true_p, conf, p_labels, labels):
    """
    Compute the average precision, given the recall and precision curves.
    
    Source: https://github.com/rafaelpadilla/Object-Detection-Metrics.
    
    params:
    -true_p:    (list) True positives .
    -conf:  (list) Objectness value from 0-1
    -p_labels: (list) Predicted object classes
    -labels: (list) True object classes

    returns:
        The average precision as computed in py-faster-rcnn.
    """

    # sort by objectness
    i = np.argsort(-conf)
    true_p, conf, p_labels = true_p[i], conf[i], p_labels[i]

    # find unique classes
    unq_classes = np.unique(labels)

    # create Precision-Recall curve and compute AP for each class
    ap, p, r = [], [], []
    for c in tqdm.tqdm(unq_classes, desc="Computing AP"):
        i = p_labels == c
        n_gt = (labels == c).sum()  # number of ground truth objects
        n_p = i.sum()  # number of predicted objects

        if n_p == 0 and n_gt == 0:
            continue
        elif n_p == 0 or n_gt == 0:
            ap.append(0)
            r.append(0)
            p.append(0)
        else:
            # accumulate FPs and TPs
            fpc = (1 - true_p[i]).cumsum()
            tpc = (true_p[i]).cumsum()

            # Recall
            recall_curve = tpc / (n_gt + 1e-16)
            r.append(recall_curve[-1])

            # Precision
            precision_curve = tpc / (tpc + fpc)
            p.append(precision_curve[-1])

            # AP from recall-precision curve
            ap.append(compute_ap(recall_curve, precision_curve))

    # compute F1 score (harmonic mean of precision and recall)
    p, r, ap = np.array(p), np.array(r), np.array(ap)
    f1 = 2 * p * r / (p + r + 1e-16)

    return p, r, ap, f1, unq_classes.astype("int32")


def compute_ap(recall, precision):
    """
    Compute the average precision, given the recall and precision curves.
    
    Source: https://github.com/rbgirshick/py-faster-rcnn.

    params:
        recall:    The recall curve (list).
        precision: The precision curve (list).
    
    returns:
        The average precision as computed in py-faster-rcnn.
    """
    # correct AP calculation
    # first append sentinel values at the end
    mrec = np.concatenate(([0.0], recall, [1.0]))
    mpre = np.concatenate(([0.0], precision, [0.0]))

    # compute the precision envelope
    for i in range(mpre.size - 1, 0, -1):
        mpre[i - 1] = np.maximum(mpre[i - 1], mpre[i])

    # to calculate area under PR curve, look for points
    # where X axis (recall) changes value
    i = np.where(mrec[1:] != mrec[:-1])[0]

    # and sum (\Delta recall) * prec
    ap = np.sum((mrec[i + 1] - mrec[i]) * mpre[i + 1])
    return ap


def get_batch_stats(detections, targets, iou_thres):
    """
    Compute true positives, predicted scores and predicted labels per sample.

    params:
    - detections: (Tensor) [nS, nBB, 5 + nC]
                  where, nBB = # bounding boxes (10647)
                  10647 = 3x[13x13 + 26x26 + 52x52]

    - targets: (Tensor) [#bboxes in batch, 6]
               where 6=[img_id in batch, label, x1, y1, x2, y2]

    - iou_thres: (float) iou threshold
    
    returns:
    - batch_metrics: (list) of true positives, pred scores, and
                     pred labels for the given batch    
    """
    batch_metrics = list()

    for sample_i in range(len(detections)):

        if detections[sample_i] is None:
            continue

        detection = detections[sample_i]
        pred_boxes = detection[:, :4]
        pred_scores = detection[:, 4]
        pred_labels = detection[:, -1]

        # true positives
        true_p = np.zeros(pred_boxes.shape[0])

        annotations = targets[targets[:, 0] == sample_i][:, 1:]
        target_labels = annotations[:, 0] if len(annotations) else []
        
        if len(annotations):
            detected_boxes = list()
            target_boxes = annotations[:, 1:]

            for pred_i, (pred_box, pred_label) in enumerate(zip(pred_boxes, pred_labels)):

                # if targets are found break
                if len(detected_boxes) == len(annotations):
                    break

                # ignore if label is not one of the target labels
                if pred_label not in target_labels:
                    continue

                iou, box_index = bbox_iou(pred_box.unsqueeze(0), target_boxes).max(0)
                if iou >= iou_thres and box_index not in detected_boxes:
                    true_p[pred_i] = 1
                    detected_boxes += [box_index]
        
        batch_metrics.append([true_p, pred_scores, pred_labels])

    return batch_metrics

def bbox_iou(box1, box2, x1y1x2y2=True):
    """
    Compute IoU between box1 and box2.

    params:
    - box1: (Tensor) bbox with size [..., 4]
    - box2: (Tensor) bbox with size [..., 4]

    returns:
    -iou: (float) IoU b/w box1 and box2
    """
    if not x1y1x2y2:
        # transform from center and width to exact coordinates
        b1_x1, b1_x2 = box1[:, 0] - box1[:, 2] / 2, box1[:, 0] + box1[:, 2] / 2
        b1_y1, b1_y2 = box1[:, 1] - box1[:, 3] / 2, box1[:, 1] + box1[:, 3] / 2
        b2_x1, b2_x2 = box2[:, 0] - box2[:, 2] / 2, box2[:, 0] + box2[:, 2] / 2
        b2_y1, b2_y2 = box2[:, 1] - box2[:, 3] / 2, box2[:, 1] + box2[:, 3] / 2
    else:
        # get the coordinates of bounding boxes
        b1_x1, b1_y1, b1_x2, b1_y2 = box1[:, 0], box1[:, 1], box1[:, 2], box1[:, 3]
        b2_x1, b2_y1, b2_x2, b2_y2 = box2[:, 0], box2[:, 1], box2[:, 2], box2[:, 3]

    # get the corrdinates of the intersection rectangle
    inter_rect_x1 = torch.max(b1_x1, b2_x1)
    inter_rect_y1 = torch.max(b1_y1, b2_y1)
    inter_rect_x2 = torch.min(b1_x2, b2_x2)
    inter_rect_y2 = torch.min(b1_y2, b2_y2)
    
    # intersection area
    inter_area = torch.clamp(inter_rect_x2 - inter_rect_x1 + 1, min=0) * torch.clamp(
        inter_rect_y2 - inter_rect_y1 + 1, min=0
    )
    # Union Area
    b1_area = (b1_x2 - b1_x1 + 1) * (b1_y2 - b1_y1 + 1)
    b2_area = (b2_x2 - b2_x1 + 1) * (b2_y2 - b2_y1 + 1)

    iou = inter_area / (b1_area + b2_area - inter_area + 1e-16)

    return iou


def non_max_suppression(predictions, conf_thres=0.5, nms_thres=0.4):
    """
    Removes detections with lower object confidence score than
    'conf_thres' and performs Non-Maximum Suppression to further
    filter detections.
    
    Performs non-max suppression.
     1. Get detection with particular class
     2. Sort by confidence
     3. Suppress non-max detection
    
    params:
    - predictions: (Tensor) [nS, nBB, 5 + nC]
                    where, nBB = # bounding boxes (10647)
                    10647 = 3x[13x13 + 26x26 + 52x52]

    - conf_thres: (float) fore-ground confidence threshold
    - nms_thres: (float) nms threshold
      
    returns:
    - detections: (list) of bboxes with 
                  bboxes = [x1, y1, x2, y2, object_conf, 
                            class_score, class_pred]
    """

    # from (center x, center y, width, height) to (x1, y1, x2, y2)
    predictions[..., :4] = xywh2xyxy(predictions[..., :4])
    output = [None for _ in range(len(predictions))]
    for image_i, image_pred in enumerate(predictions):
        # filter out confidence scores below threshold
        image_pred = image_pred[image_pred[:, 4] >= conf_thres]
        # if none are remaining => process next image
        if not image_pred.size(0):
            continue
        # object confidence times class confidence
        score = image_pred[:, 4] * image_pred[:, 5:].max(1)[0]
        
        # sort by it
        image_pred = image_pred[(-score).argsort()]
        
        class_confs, class_preds = image_pred[:, 5:].max(1, keepdim=True)
        detections = torch.cat((image_pred[:, :5], class_confs.float(), class_preds.float()), 1)
        # perform non-maximum suppression
        keep_boxes = list()
        while detections.size(0):
            large_overlap = bbox_iou(detections[0, :4].unsqueeze(0), detections[:, :4]) > nms_thres
            label_match = detections[0, -1] == detections[:, -1]
            # indices of boxes with lower confidence scores, large IOUs and matching labels
            invalid = large_overlap & label_match
            weights = detections[invalid, 4:5]
            # merge overlapping bboxes by order of confidence
            detections[0, :4] = (weights * detections[invalid, :4]).sum(0) / weights.sum()
            keep_boxes += [detections[0]]
            detections = detections[~invalid]
        if keep_boxes:
            output[image_i] = torch.stack(keep_boxes)

    return output