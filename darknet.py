'''
may 6th 2019 monday
'''
from __future__ import division

import torch
import torch.nn as nn
import torch.nn.functional as F
from torch.autograd import Variable
import numpy as np

from parse_config import *
from utils import to_cpu, non_max_suppression

import matplotlib.pyplot as plt
import matplotlib.patches as patches

def create_modules(module_defs):
    """
    Constructs module list of layer blocks from module configuration.

    params:
    - module_defs: (list) of module defs stored as dict

    returns:
    - hyperparams: (dict) defining the network hyperparams
    - module_list: (nn.ModuleList) module list of neural
                   network 
    """
    hyperparams = module_defs.pop(0)
    output_filters = [int(hyperparams["channels"])]
    module_list = nn.ModuleList()
    for module_i, module_def in enumerate(module_defs):
        modules = nn.Sequential()

        if module_def["type"] == "convolutional":
            bn = int(module_def["batch_normalize"])
            filters = int(module_def["filters"])
            kernel_size = int(module_def["size"])
            
            pad = (kernel_size - 1) // 2
            conv = nn.Conv2d(in_channels=output_filters[-1], out_channels=filters,
                             kernel_size=kernel_size, stride=int(module_def["stride"]),
                             padding=pad, bias=not bn)
            
            modules.add_module(f"conv_{module_i}", conv)
                
            if bn:
                modules.add_module(f"batch_norm_{module_i}", 
                                  nn.BatchNorm2d(filters, momentum=0.9, eps=1e-5))
            
            if module_def["activation"] == "leaky":
                modules.add_module(f"leaky_{module_i}", nn.LeakyReLU(0.1))

        elif module_def["type"] == "maxpool":
            kernel_size = int(module_def["size"])
            stride = int(module_def["stride"])
            if kernel_size == 2 and stride == 1:
                pad = int((kernel_size - 1) // 2)
                modules.add_module(f"_debug_padding_{module_i}", nn.ZeroPad2d((0, 1, 0, 1)))
            maxpool = nn.MaxPool2d(kernel_size=kernel_size, stride=stride, padding=pad)
            modules.add_module(f"maxpool_{module_i}", maxpool)

        elif module_def["type"] == "upsample":
            upsample = Upsample(scale_factor=int(module_def["stride"]), mode="nearest")
            modules.add_module(f"upsample_{module_i}", upsample)

        elif module_def["type"] == "route":
            layers = [int(x) for x in module_def["layers"].split(",")]
            filters = sum([output_filters[1:][i] for i in layers])
            modules.add_module(f"route_{module_i}", EmptyLayer())

        elif module_def["type"] == "shortcut":
            filters = output_filters[1:][int(module_def["from"])]
            modules.add_module(f"shortcut_{module_i}", EmptyLayer())

        elif module_def["type"] == "yolo":
            anchor_idxs = [int(x) for x in module_def["mask"].split(",")]
            # extract anchors
            anchors = [int(x) for x in module_def["anchors"].split(",")]
            anchors = [(anchors[i], anchors[i + 1]) for i in range(0, len(anchors), 2)]
            anchors = [anchors[i] for i in anchor_idxs]
            
            nC = int(module_def["classes"])
            img_res = int(hyperparams["height"])
            
            # define detection layer
            yolo_layer = YOLOLayer(anchors, nC, img_res)
            modules.add_module(f"yolo_{module_i}", yolo_layer)
        
        # register module list and number of output filters
        module_list.append(modules)
        output_filters.append(filters)
        
    return hyperparams, module_list

class Upsample(nn.Module):
    """
    nn.Upsample is deprecated.
    """

    def __init__(self, scale_factor, mode="nearest"):
        super(Upsample, self).__init__()
        self.scale_factor = scale_factor
        self.mode = mode

    def forward(self, x):
        x = F.interpolate(x, scale_factor=self.scale_factor, mode=self.mode)
        return x


class EmptyLayer(nn.Module):
    """
    Empty layer for shortcut connection.
    """

    def __init__(self):
        super(EmptyLayer, self).__init__()

class YOLOLayer(nn.Module):
    """
    Detection layer
    
    params:
    - anchors: (list) of tuples containing anchors(w, h)
    - nC: (int) num of classes in the dataset
    - img_res: (int) original image resolution
    """

    def __init__(self, anchors, nC, img_res=416):
        super(YOLOLayer, self).__init__()
        self.anchors = anchors
        self.nA = len(anchors)
        self.nC = nC
        self.gs = 0  # grid size
        self.img_res = img_res

    def compute_grid_offsets(self, gs, cuda=True):
        self.gs = gs
        gs = self.gs
        FloatTensor = torch.cuda.FloatTensor if cuda else torch.FloatTensor
        self.stride = self.img_res / self.gs
        # calculate offsets for each grid
        self.grid_x = torch.arange(gs).repeat(gs, 1).view([1, 1, gs, gs]).type(FloatTensor)
        self.grid_y = torch.arange(gs).repeat(gs, 1).t().view([1, 1, gs, gs]).type(FloatTensor)
        self.scaled_anchors = FloatTensor([(a_w / self.stride, a_h / self.stride) for a_w, a_h in self.anchors])
        self.anchor_w = self.scaled_anchors[:, 0:1].view((1, self.nA, 1, 1))
        self.anchor_h = self.scaled_anchors[:, 1:2].view((1, self.nA, 1, 1))

    def forward(self, x, img_res=None):
        """
        Transform feature map into 2-D tensor. Transformation includes:
            1. Re-organize tensor to make each row correspond to a bbox
            2. Transform center coordinates
                bx = sigmoid(tx) + cx
                by = sigmoid(ty) + cy
            3. Transform width and height
                bw = pw * exp(tw)
                bh = ph * exp(th)
            4. Activation
        
        params:
        -x: (Tensor) feature map [nS, (5 + nC) * nA, gs, gs]
            5 => [4 offsets (xc, yc, w, h), objectness_score]
        
        -img_res: (int) original image resolution
        
        returns:
        - detections: (Tensor) feature map with
                      size [nS, nA, gs, gs, 5 + nC]
        - loss: (int) Since we aren't training the detector,
                it returns 0
        """
        # tensors for cuda support
        FloatTensor = torch.cuda.FloatTensor if x.is_cuda else torch.FloatTensor
        LongTensor = torch.cuda.LongTensor if x.is_cuda else torch.LongTensor
        ByteTensor = torch.cuda.ByteTensor if x.is_cuda else torch.ByteTensor

        self.img_res = img_res
        nS = x.size(0)
        gs = x.size(2)

        prediction = (
            x.view(nS, self.nA, self.nC + 5, gs, gs)
            .permute(0, 1, 3, 4, 2)
            .contiguous()
        )

        # get outputs
        cx = torch.sigmoid(prediction[..., 0])  # center x
        cy = torch.sigmoid(prediction[..., 1])  # center y
        w = prediction[..., 2]  # width
        h = prediction[..., 3]  # height
        pred_conf = torch.sigmoid(prediction[..., 4])  # conf
        pred_cls = torch.sigmoid(prediction[..., 5:])  # cls pred

        # if grid size does not match current we compute new offsets
        if gs != self.gs:
            self.compute_grid_offsets(gs, cuda=x.is_cuda)

        # add offset and scale with anchors
        pred_boxes = FloatTensor(prediction[..., :4].shape)
        pred_boxes[..., 0] = cx.data + self.grid_x
        pred_boxes[..., 1] = cy.data + self.grid_y
        pred_boxes[..., 2] = torch.exp(w.data) * self.anchor_w
        pred_boxes[..., 3] = torch.exp(h.data) * self.anchor_h

        detections = torch.cat(
            (
                pred_boxes.view(nS, -1, 4) * self.stride,
                pred_conf.view(nS, -1, 1),
                pred_cls.view(nS, -1, self.nC),
            ),
            -1,
        )

        return detections, 0
        

class Darknet(nn.Module):
    """
    YOLOv3 object detection model.

    params:
    - cfg_file: (str) path to models's .cfg file
    - img_res: (int) original image resolution
    """

    def __init__(self, cfg_file, img_res=416):
        super(Darknet, self).__init__()
        self.module_defs = parse_model_config(cfg_file)
        self.hyperparams, self.module_list = create_modules(self.module_defs)
        self.img_res = img_res
        self.seen = 0

        """ NOTE:
        The first 5 values are header information 
         1. Major version number
         2. Minor Version Number
         3. Subversion number 
         4. Images seen by the network (during training)
         5. Images seen by the network (during training) if overflow
        """
        self.header_info = np.array([0, 0, 0, self.seen, 0], dtype=np.int32)

    def forward(self, x):
        """
        Forward pass of Darknet.

        params:
        - x: (Tensor) input Tensor, with size[nS, c, h, w],
             where, c = # of channels

        returns:
        - detections: (Tensor) [nS, nBB, 5 + nC]
                      where, nBB = # bounding boxes (10647)
                      10647 = 3x[13x13 + 26x26 + 52x52]
        """

        img_res = x.shape[2]
        loss = 0
        layer_outputs, yolo_outputs = [], []
        for i, (module_def, module) in enumerate(zip(self.module_defs, self.module_list)):
            if module_def["type"] in ["convolutional", "upsample", "maxpool"]:
                x = module(x)
            
            elif module_def["type"] == "route":
                layers = module_def["layers"].split(",")
                x = torch.cat([layer_outputs[int(layer_i)] for layer_i in layers], 1)
            
            elif module_def["type"] == "shortcut":
                layer_i = int(module_def["from"])
                x = layer_outputs[-1] + layer_outputs[layer_i]
            
            elif module_def["type"] == "yolo":
                x, layer_loss = module[0](x, img_res)
                loss += layer_loss
                yolo_outputs.append(x)
            layer_outputs.append(x)
        yolo_outputs = to_cpu(torch.cat(yolo_outputs, 1))
        
        return yolo_outputs

    def load_darknet_weights(self, weights_path):
        """
        Load darknet weights from .weights file. Since, YOLOv3 
        is fully convolutional, so only conv layers' weights
        will be loaded. 
        
        Darknet's weights data are organized as:
         1. (optional) bn_biases => bn_weights => bn_mean => bn_var
         1. (optional) conv_bias
         2. conv_weights

        params:
        - weights_path: (str) path to .weights file
        """

        # Open the weights file
        with open(weights_path, "rb") as f:
            header = np.fromfile(f, dtype=np.int32, count=5)  # first five are header values
            self.header_info = header  # need to write header when saving weights
            self.seen = header[3]  # number of images seen during training
            weights = np.fromfile(f, dtype=np.float32)  # the rest are weights

        # establish cutoff for loading backbone weights
        cutoff = None
        if "darknet53.conv.74" in weights_path:
            cutoff = 75

        ptr = 0
        for i, (module_def, module) in enumerate(zip(self.module_defs, self.module_list)):
            if i == cutoff:
                break
            if module_def["type"] == "convolutional":
                conv_layer = module[0]
                if module_def["batch_normalize"]:
                    # load BN bias, weights, running mean and running variance
                    bn_layer = module[1]
                    num_b = bn_layer.bias.numel()  # number of biases
                    
                    # bias
                    bn_b = torch.from_numpy(weights[ptr : ptr + num_b]).view_as(bn_layer.bias)
                    bn_layer.bias.data.copy_(bn_b)
                    ptr += num_b
                    
                    # weight
                    bn_w = torch.from_numpy(weights[ptr : ptr + num_b]).view_as(bn_layer.weight)
                    bn_layer.weight.data.copy_(bn_w)
                    ptr += num_b
                    
                    # running Mean
                    bn_rm = torch.from_numpy(weights[ptr : ptr + num_b]).view_as(bn_layer.running_mean)
                    bn_layer.running_mean.data.copy_(bn_rm)
                    ptr += num_b
                    
                    # wunning Var
                    bn_rv = torch.from_numpy(weights[ptr : ptr + num_b]).view_as(bn_layer.running_var)
                    bn_layer.running_var.data.copy_(bn_rv)
                    ptr += num_b
                else:
                    # load conv. bias
                    num_b = conv_layer.bias.numel()
                    conv_b = torch.from_numpy(weights[ptr : ptr + num_b]).view_as(conv_layer.bias)
                    conv_layer.bias.data.copy_(conv_b)
                    ptr += num_b
                
                # load conv. weights
                num_w = conv_layer.weight.numel()
                conv_w = torch.from_numpy(weights[ptr : ptr + num_w]).view_as(conv_layer.weight)
                conv_layer.weight.data.copy_(conv_w)
                ptr += num_w